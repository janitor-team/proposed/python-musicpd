.. SPDX-FileCopyrightText: 2018-2021  kaliko <kaliko@azylum.org>
.. SPDX-License-Identifier: GPL-3.0-or-later

musicpd namespace
=================

.. autodata:: musicpd.CONNECTION_TIMEOUT

.. autodata:: musicpd.SOCKET_TIMEOUT

.. autoclass:: musicpd.MPDClient
    :members:


.. vim: spell spelllang=en
