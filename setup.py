#! /usr/bin/env python3
# coding: utf-8
# SPDX-FileCopyrightText: 2012-2021  kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

from setuptools import setup

setup()

# vim: set expandtab shiftwidth=4 softtabstop=4 textwidth=79:
